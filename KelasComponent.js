export const KelasComponent = {
    data () {
        return {
            kelas: [
                {
                    id: 1,
                    title: 'Kelas Pagi'
                },
                {
                    id: 2,
                    title: 'Kelas Siang'
                },
                {
                    id: 3,
                    title: 'Kelas Malam'
                },
                
            ]
        }
    },
    template: `
        <div>
            <h1>Daftar Kelas</h1>
            <ul>
                <li v-for="jurusan of kelas">
                    <router-link :to="'/jurusan/'+jurusan.id"> 
                        {{ jurusan.title }} 
                    </router-link>
                </li>
            </ul>
        </div>
    ` 
}