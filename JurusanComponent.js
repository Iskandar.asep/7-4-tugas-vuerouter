export const JurusanComponent = {
    data(){
        return {
            kelas: [
                {
                    id: 1,
                    title: 'Kelas Pagi',
                    description : 'Kelas Pagi : Sistem Infomasi, Teknik Infomatika, Multimedia.'
                },
                {
                    id: 2,
                    title: 'Kelas Siang',
                    description : 'Kelas Siang : Akuntansi , Teknik Infomatika, Multimedia.'

                },
                {
                    id: 3,
                    title: 'Kelas Malam',
                    description : 'Kelas Malam : Sistem Infomasi, Teknik Infomatika, Akuntansi.'

                },
                
            ]
        }
    },
    computed: {
        jurusan() {
            return this.kelas.filter((jurusan)=>{
                return jurusan.id === parseInt(this.$route.params.id)                
            })[0]
        }
    },
    template: `<div >
            <h1>jurusan :  {{ jurusan.title }}</h1>
            <ul>
                <li v-for="(num, value) of jurusan">
                    {{ num +' : '+ value }} <br>
                </li>
            </ul>
        </div>`,
   
}